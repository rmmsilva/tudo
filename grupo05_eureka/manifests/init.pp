# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include grupo05_eureka
class grupo05_eureka {
  group { 'java':
       ensure => 'present',
       gid    => '501',
     }

  user { 'java':
       ensure           => 'present',
       gid              => '501',
       home             => '/home/java',
       password         => '!!',
       password_max_age => '99999',
       password_min_age => '0',
       shell            => '/bin/bash',
       uid              => '501',
     }

  file { '/opt/apps':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }

  file { '/opt/apps/eureka':
        ensure => directory,
        owner => 'java',
        group => 'java',
    }

  exec { 'eureka-get':
    command => "/usr/bin/wget -q http://23.96.48.125/artfactory/grupo5/eureka/eureka.jar -O /opt/apps/grupo05/eureka/eureka.jar",
    creates => "/opt/apps/grupo05/eureka/eureka.jar" 
  }
  
  file { '/opt/apps/grupo05/eureka/eureka.jar':
    mode => '0755',
    require => Exec[ 'eureka-get' ]
  }

}
